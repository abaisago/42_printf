/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 20:26:45 by abaisago          #+#    #+#             */
/*   Updated: 2019/04/01 17:25:42 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"

#include "curly.h"
#include "percent.h"

#include <unistd.h>

static void		choose(va_list og, va_list ap, char **str, t_buf *buf)
{
	if (**str == '%')
		percent(og, ap, str, buf);
	else if ((*str)[1] == '{')
	{
		++(*str);
		buf->data[buf->index++] = **str;
		buf->read += 1;
	}
	else if (**str == '{')
		curly(str, buf);
}

int				parsing(va_list ap, char *str)
{
	va_list		og;
	t_buf		buf;

	buf.index = 0;
	buf.read = 0;
	va_copy(og, ap);
	while (*str != '\0')
	{
		if (buf.index == BUFF_SIZE)
			print_buf(&buf);
		if (*str != '%' && *str != '{')
		{
			buf.data[buf.index++] = *str;
			buf.read += 1;
		}
		else
			choose(og, ap, &str, &buf);
		++str;
	}
	write(1, buf.data, buf.index);
	return (buf.read);
}
